import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

import { TherapistsService } from './therapists.service';

import { TherapistsRoutingModule } from './therapists-routing.module';
import { TherapistsViewComponent } from './therapists-view/therapists-view.component';
import { TherapistDetailComponent } from './therapist-detail/therapist-detail.component';
import { FormModalComponent } from './form-modal/form-modal.component';


@NgModule({
  declarations: [TherapistsViewComponent, TherapistDetailComponent, FormModalComponent],
  imports: [
    CommonModule,
    TherapistsRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [TherapistsService]
})
export class TherapistsModule { }
