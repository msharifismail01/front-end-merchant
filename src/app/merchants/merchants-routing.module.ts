import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MerchantsHomeComponent } from './merchants-home/merchants-home.component';

const routes: Routes = [
  {component: MerchantsHomeComponent, path: 'merchants-home'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MerchantsRoutingModule { }
