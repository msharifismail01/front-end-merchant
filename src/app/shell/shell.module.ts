import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShellRoutingModule } from './shell-routing.module';
import { HeaderComponent } from './header/header.component';
import { RatingComponent } from './rating/rating.component';


@NgModule({
  declarations: [HeaderComponent, RatingComponent],
  imports: [
    CommonModule,
    ShellRoutingModule,
  ],
  exports: [
    HeaderComponent,
    RatingComponent
  ]
})
export class ShellModule { }
