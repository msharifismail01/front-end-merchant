import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';

import { RatingComponent } from './../../shell/rating/rating.component';
import { FormModalComponent } from './../form-modal/form-modal.component';
import { TherapistsService } from './../therapists.service';
import { Therapists } from './../../shared/models/therapists';

@Component({
  selector: 'app-therapists-view',
  templateUrl: './therapists-view.component.html',
  styleUrls: ['./therapists-view.component.scss']
})
export class TherapistsViewComponent implements OnInit {

  therapists: Therapists[] = [];
  directives: [RatingComponent];

  constructor(
    private service: TherapistsService,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this.therapists = this.service.read();
  }

  // tslint:disable-next-line: typedef
  openFormModal() {
    const modalRef = this.modalService.open(FormModalComponent);
    modalRef.componentInstance.id = 10; // should be the id

    modalRef.result.then((result) => {
      console.log(result);
    }).catch((error) => {
      console.log(error);
    });
  }
}
