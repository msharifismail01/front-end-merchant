import { Injectable } from '@angular/core';
import { Products, MOCK_PRODUCTS } from '../shared/models/products';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor() { }

  read(): Array<Products>{
    return MOCK_PRODUCTS;
  }

  readOne(id: number): Products{
    const oneProduct = MOCK_PRODUCTS.find(e => e.id = id);
    return oneProduct;
  }
}
