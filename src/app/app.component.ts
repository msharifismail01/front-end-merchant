import { Component, OnInit } from '@angular/core';
import { FirebaseService } from './firebase.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'merchant-app';
  email: string;
  password: string;
  constructor(public firebaseService: FirebaseService){}

  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }

  signup() {
    this.firebaseService.signup(this.email, this.password);
    this.email = this.password = '';
  }

  login() {
    this.firebaseService.login(this.email, this.password);
    this.email = this.password = '';
  }

  logout() {
    this.firebaseService.logout();
  }
}
