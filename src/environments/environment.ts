// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false ,
  firebase: {
    apiKey: 'AIzaSyC3tRLCsfK3YTzSOPappZ_5z0p75jMznVg',
      authDomain: 'snth-angular-fireauth.firebaseapp.com',
      databaseURL: 'https://snth-angular-fireauth.firebaseio.com',
      projectId: 'snth-angular-fireauth',
      storageBucket: 'snth-angular-fireauth.appspot.com',
      messagingSenderId: '862923656687',
      appId: '1:862923656687:web:484ba1c3c30633f695ba1a',
      measurementId: 'G-16L4E6LNCE',
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
