import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-form-modal',
  templateUrl: './form-modal.component.html',
  styleUrls: ['./form-modal.component.scss']
})
export class FormModalComponent implements OnInit {

  @Input() id: number;
  @Input() action: string;
  myForm: FormGroup;

  constructor(
    public activeModal: NgbActiveModal,
    private formBuilder: FormBuilder) { this.createForm(); }

  ngOnInit(): void {
  }

  // tslint:disable-next-line: typedef
  closeModal() {
    this.activeModal.close('Modal Closed');
  }

  private createForm() {
    this.myForm = this.formBuilder.group({
      ProductName: ['', Validators.required ],
      ProductPrice: ['', Validators.required ],
      ProductAmount: ['', Validators.required ],
    });
  }
  // private submitForm() {
  //   this.activeModal.close(this.myForm.value);
  // }

  // Condition , untuk asingkan bezakan dia update atau create
  // Add panggil service create
  // D

}
