import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireAuthGuard, redirectUnauthorizedTo } from '@angular/fire/auth-guard';

import { RegisterComponent } from './login/register/register.component';
import { LoginFormComponent } from './login/login-form/login-form.component';
import { MerchantsHomeComponent } from './merchants/merchants-home/merchants-home.component';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['login']);

const routes: Routes = [
  // tslint:disable-next-line: max-line-length
  { path: 'login', component: LoginFormComponent, canActivate: [AngularFireAuthGuard], data: { authGuardPipe: redirectUnauthorizedToLogin } },
  { path: 'register', component: RegisterComponent },
  {
    path: '',
    component: MerchantsHomeComponent,
    canActivate: [AngularFireAuth]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
