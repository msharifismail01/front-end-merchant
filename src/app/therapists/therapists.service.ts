import { Injectable } from '@angular/core';

import { Therapists, MOCK_THERAPISTS } from './../shared/models/therapists';

@Injectable({
  providedIn: 'root'
})
export class TherapistsService {

  constructor() { }

  read(): Array<Therapists>{
    return MOCK_THERAPISTS;
  }

  readOne(id: number): Therapists {
    const oneTherapist = MOCK_THERAPISTS.find(e => e.id = id);
    return oneTherapist;
  }
}
