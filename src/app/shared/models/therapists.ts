export interface Therapists {
  id: number;
  name: string;
  phone_number: string;
  ic: string;
  state: string;
  city: string;
  date_join: Date;
}

// mock data
export const MOCK_THERAPISTS: Array<Therapists> = [
  //
  {id: 1, name: 'Ali', phone_number: '010', ic: '9701', state: 'selangor', city: 'kajang', date_join: new Date()},
  {id: 2, name: 'Abu', phone_number: '011', ic: '9702', state: 'selangor', city: 'klang', date_join: new Date()},
  {id: 3, name: 'Aah', phone_number: '012', ic: '9703', state: 'selangor', city: 'banting', date_join: new Date()},
];
