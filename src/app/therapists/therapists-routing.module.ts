import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TherapistsViewComponent } from './therapists-view/therapists-view.component';
import { TherapistDetailComponent } from './therapist-detail/therapist-detail.component';

const routes: Routes = [
  {component: TherapistsViewComponent, path: 'therapists'},
  {component: TherapistDetailComponent, path: 'therapists/:id'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TherapistsRoutingModule { }
