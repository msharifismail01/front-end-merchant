import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductsViewComponent } from './products-view/products-view.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';


const routes: Routes = [
  {component: ProductsViewComponent, path: 'products'},
  {component: ProductDetailComponent, path: 'products/:id'}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
