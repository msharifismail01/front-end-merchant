import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

import { ProductsService } from './products.service';

import { ProductsRoutingModule } from './products-routing.module';
import { ProductsViewComponent } from './products-view/products-view.component';
import { ProductDetailComponent } from './product-detail/product-detail.component';
import { FormModalComponent } from './form-modal/form-modal.component';


@NgModule({
  declarations: [ProductsViewComponent, ProductDetailComponent, FormModalComponent],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [ProductsService]
})
export class ProductsModule { }
