import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss']
})
export class RatingComponent implements OnInit {

  constructor() { }

  public range: Array<number> = [1, 2, 3, 4, 5];

  // tslint:disable-next-line: no-inferrable-types
  public rating: number = 3;

  ngOnInit(): void {
  }

}
