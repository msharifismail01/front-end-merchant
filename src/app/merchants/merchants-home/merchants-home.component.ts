import { AngularFireAuth } from '@angular/fire/auth';
import { FirebaseService } from './../../firebase.service';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-merchants-home',
  templateUrl: './merchants-home.component.html',
  styleUrls: ['./merchants-home.component.scss']
})
export class MerchantsHomeComponent implements OnInit {

  @Output() isLogout = new EventEmitter<void>();
  constructor(
    public firebaseService: FirebaseService,
    private auth: AngularFireAuth
    ) { }

  ngOnInit(): void {
  }

  // tslint:disable-next-line: typedef
  logout(){
    // this.firebaseService.logout();
    this.auth.signOut();
  }

}
