import { Products } from 'src/app/shared/models/products';
import { ProductsService } from './../products.service';
import { Component, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { FormModalComponent } from '../form-modal/form-modal.component';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss'],
})
export class ProductDetailComponent implements OnInit {

  constructor(private service: ProductsService,
              private route: ActivatedRoute,
              private modalService: NgbModal, ) { }

  product: Products;

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.product = this.service.readOne(params.id);
      console.log(this.product);
    });

  }

  openFormModal() {
    const modalRef = this.modalService.open(FormModalComponent);
    modalRef.componentInstance.id = 10; // should be the id

    modalRef.result.then((result) => {
      console.log(result);
    }).catch((error) => {
      console.log(error);
    });
  }

}
