import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Component, OnInit } from '@angular/core';

import { FormModalComponent } from './../form-modal/form-modal.component';
import { ProductsService } from './../products.service';
import { Products } from 'src/app/shared/models/products';

@Component({
  selector: 'app-products-view',
  templateUrl: './products-view.component.html',
  styleUrls: ['./products-view.component.scss']
})
export class ProductsViewComponent implements OnInit {

  products: Products[] = [];

  constructor(private service: ProductsService,
              private modalService: NgbModal
    ) { }

  ngOnInit(): void {
    this.products = this.service.read();
  }

  // tslint:disable-next-line: typedef
  openFormModal() {
    const modalRef = this.modalService.open(FormModalComponent);
    modalRef.componentInstance.id = 10; // should be the id

    modalRef.result.then((result) => {
      console.log(result);
    }).catch((error) => {
      console.log(error);
    });
  }

}
