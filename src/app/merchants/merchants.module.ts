import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MerchantsRoutingModule } from './merchants-routing.module';
import { MerchantsHomeComponent } from './merchants-home/merchants-home.component';


@NgModule({
  declarations: [MerchantsHomeComponent],
  imports: [
    CommonModule,
    MerchantsRoutingModule
  ],
  exports: [
    MerchantsHomeComponent
  ]
})
export class MerchantsModule { }
