export interface Products {
  id: number;
  name: string;
  price: number;
  description: string;
  date_created: Date;
  date_updated: Date;
  rating: number;
  voucher: number;
}

// mock data
export const MOCK_PRODUCTS: Array<Products> = [
  // tslint:disable-next-line: max-line-length
  {id: 1, name: 'sabun', price: 5, description: 'sabun di bilik mandi', date_created: new Date(), date_updated: new Date(), rating: 5, voucher: 1},
  // tslint:disable-next-line: max-line-length
  {id: 2, name: 'sabun baju', price: 5, description: 'sabun di bilik mandi', date_created: new Date(), date_updated: new Date(), rating: 4, voucher: 2},
  // tslint:disable-next-line: max-line-length
  {id: 3, name: 'sabun tandas', price: 5, description: 'sabun di bilik mandi', date_created: new Date(), date_updated: new Date(), rating: 3, voucher: 4},

];

// add amount
